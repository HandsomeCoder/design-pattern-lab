
# CSCI 5308 - Advance Topic in Software Developer 
# Lab 11 - Design Pattern

## Getting started

### Prerequisites

- Install Maven (https://maven.apache.org/install.html)

## Problem 1 - Charger
Mobile battery needs 3 volts to charge but the normal socket produces either 120V or 240V. 
So the mobile charger works as an adapter between mobile charging socket and the wall socket.

## Problem 2 - Messenger
Simple topic and observers can register to this topic. Whenever any new message will be posted to the topic, 
all the registers observers will be notified and they can consume the message.

## Problem 3 - Shopper
Shopping Cart where we have two payment strategies - using Credit Card or using PayPal.

## Challenge - File System
File System utility with methods to open, write and close file. 
This file system utility should support multiple operating systems such as Windows and Unix.